#ifndef _ASSERT_H_
#define _ASSERT_H_

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

static inline void __panic_assert(const char* file, int line, const char* d) {
	printf("panic: %s:%d\n", file, line);
	printf("assertion failed: %s\n", d);
	exit(1);
}

static inline void
__panic_assert3(const char* file, int line, const char *c_a, uint32_t v_a,
				const char* op, const char *c_b, uint32_t v_b)
{
	printf("panic: %s:%d\n", file, line);
	printf("assertion failed: %s %s %s\n", c_a, op, c_b);
	printf("%s=%08x, %s=%08x\n", c_a, v_a, c_b, v_b);
	exit(1);
}

#define ASSERT(b) ((b) ? (void)0: __panic_assert(__FILE__, __LINE__, #b))
#define ASSERT3(a, op, b) \
	((a op b) ? (void)0 : __panic_assert3(__FILE__, __LINE__, #a, a, #op, #b, b))

#endif /* _ASSERT_H_ */
