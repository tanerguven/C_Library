/*
 * Copyright (C) 2013,2014 Taner Guven <tanerguven@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <assert.h>

#include "subprocess.h"

int subprocess_fclose(struct subprocess *p, FILE *fd) {
	if (fd == NULL || fd == stdin || fd == stdout || fd == stderr)
		return -1;
	if (p->stdin == fd) {
		p->stdin = NULL;
		fclose(fd);
		return 0;
	}
	if (p->stdout == fd) {
		p->stdout = NULL;
		fclose(fd);
		return 0;
	}
	if (p->stderr == fd) {
		p->stderr = NULL;
		fclose(fd);
		return 0;
	}
	return -1;
}

int subprocess_free(struct subprocess *p) {
	int r;

	r = kill(p->pid, 0);
	if (r == 0 || errno != ESRCH)
		return -1;

	if (p->stdin != NULL && p->stdin != stdin)
		fclose(p->stdin);
	if (p->stdout != NULL && p->stdout != stdout)
		fclose(p->stdout);
	if (p->stderr != NULL && p->stderr != stderr)
		fclose(p->stderr);

	char **c = p->argv;
	while (*c != NULL) {
		free(*c);
		c++;
	}

	free(p->argv);

	return 0;
}

struct subprocess* subprocess_init(struct subprocess *p) {
	memset(p, 0, sizeof(struct subprocess));
	p->pid = -1;
	return p;
}

int subprocess_popen(struct subprocess *p, const char *path, char **argv) {
	int i;
	/* in: pipe[0], out: pipe[1], err: pipe[2] */
	int pipes[3][2];

	struct subprocess_file *selections = &p->_stdin_selection;
	for (i = 0 ; i < 3 ; i++) {
		if (selections[i].type == SP_FILE_PIPE) {
			if (pipe(pipes[i]) < 0)
				return -2; // FIXME: clsoe pipes
		}
	}

	p->argv = argv;
	p->pid = fork();
	if (p->pid < 0)
		return -1;

	if (p->pid == 0) {
		/* child process */

		switch(p->_stdin_selection.type) {
		case SP_FILE_PIPE:
			/* in pipe okuma tarafini stdin olarak kullan */
			close(0);
			dup2(pipes[0][0], 0);
			/* in pipenin yazma tarafini kapat */
			close(pipes[0][1]);
			break;
		case SP_FILE_NAME:
			close(0);
			FILE *f = fopen(p->_stdin_selection.data.file_name, "r");
			assert(f != NULL);
			assert(f->_fileno == 0);
			break;
		}

		switch(p->_stdout_selection.type) {
		case SP_FILE_PIPE:
			close(1);
			dup2(pipes[1][1], 1);
			close(pipes[1][0]);
			break;
		case SP_FILE_NAME:
			close(1);
			FILE *f = fopen(p->_stdout_selection.data.file_name, "w");
			assert(f != NULL);
			assert(f->_fileno == 1);
			break;
		}

		switch(p->_stderr_selection.type) {
		case SP_FILE_PIPE:
			close(2);
			dup2(pipes[2][1], 2);
			close(pipes[2][0]);
			break;
		case SP_FILE_NAME:
			close(2);
			FILE *f = fopen(p->_stderr_selection.data.file_name, "w");
			assert(f != NULL);
			assert(f->_fileno == 2);
			break;
		}

		execv(path, argv);
		/* exec calismazsa child process sonlanmali */
		exit(1);
	}

	/* parent process */

	if (p->_stdin_selection.type == SP_FILE_PIPE) {
		/* parent processde in pipe okuma tarafini kapat */
		close(pipes[0][0]);
		p->stdin = fdopen(pipes[0][1], "w");
	} else {
		p->stdin = NULL;
	}

	if (p->_stdout_selection.type == SP_FILE_PIPE) {
		/* parent processde out pipe yazma tarafini kapat */
		close(pipes[1][1]);
		p->stdout = fdopen(pipes[1][0], "r");
	} else {
		p->stdout = NULL;
	}

	if (p->_stderr_selection.type == SP_FILE_PIPE) {
		/* parent processde err pipe yazma tarafini kapat */
		close(pipes[2][1]);
		p->stderr = fdopen(pipes[2][0], "r");
	} else {
		p->stderr = NULL;
	}

	return 0;
}

int subprocess_wait(struct subprocess *p) {
	waitpid(p->pid, &p->exit_status, 0);
	if (WIFEXITED(p->exit_status) == 0)
		return -1;

	return WEXITSTATUS(p->exit_status);
}

char **subprocess_argv(const char *arg, ...) {
	va_list ap;
	const char *x;
	char ** r = (char**)malloc(sizeof(char**)*8);
	int i = 0;

	va_start(ap, arg);
	x = arg;
	while (x != NULL) {
		r[i++] = strdup(x);
		if (i % 8 == 0) {
			/* printf("realloc arg: %d\n", i); */
			r = realloc(r, sizeof(char**)*(i+8));
		}
		x = va_arg(ap, const char*);
	}
	r[i] = NULL;

	va_end(ap);
	return r;
}

/*
 * readsize+1 alanda okumaya baslar. alan yetmedigi zaman readsize kadar ekler
 * buffer maximum readsize*maxcount+1 yer kaplar
 */
char* buffered_read(int fd, size_t readsize, size_t maxcount) {
	int count = 1;
	char *buf = (char*)malloc((count*readsize+1)*sizeof(char));

	size_t i = 0;
	int r;

	do {
		r = read(fd, buf+i, readsize);
		if (r == 0)
			break;
		buf[r+i] = '\0';
		i += r;
		if (i % readsize == 0) {
			count++;
			if (count > maxcount)
				break;
			/* printf("realloc: %zu\n", count*readsize+1); */
			buf = realloc(buf, (count*readsize+1)*sizeof(char));
		}
	} while(1);

	return buf;
}
