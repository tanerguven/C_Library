/*
 * Copyright (C) 2013,2014 Taner Guven <tanerguven@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef _SUBPROCESS_H_
#define _SUBPROCESS_H_

#define SP_FILE_NAME 1
#define SP_FILE_FP 2
#define SP_FILE_PIPE 3

struct subprocess_file {
	int type;
	union {
		const char *file_name;
		FILE *fp;
	} data;
};

struct subprocess {
	FILE* stdin;
	FILE* stdout;
	FILE* stderr;

	int pid;
	char **argv;
	int exit_status;

	/* user input */
	struct subprocess_file _stdin_selection;
	struct subprocess_file _stdout_selection;
	struct subprocess_file _stderr_selection;

};

static inline void subprocess_file_selection_name(struct subprocess *p, int file, const char *name) {
	switch (file) {
	case 0:
		p->_stdin_selection.type = SP_FILE_NAME;
		p->_stdin_selection.data.file_name = name;
		break;
	case 1:
		p->_stdout_selection.type = SP_FILE_NAME;
		p->_stdout_selection.data.file_name = name;
		break;
	case 2:
		p->_stderr_selection.type = SP_FILE_NAME;
		p->_stderr_selection.data.file_name = name;
		break;
	}
}

static inline void subprocess_file_selection_fp(struct subprocess *p, int file, FILE *fp) {
	switch (file) {
	case 0:
		p->_stdin_selection.type = SP_FILE_FP;
		p->_stdin_selection.data.fp = fp;
		break;
	case 1:
		p->_stdout_selection.type = SP_FILE_FP;
		p->_stdout_selection.data.fp = fp;
		break;
	case 2:
		p->_stderr_selection.type = SP_FILE_FP;
		p->_stderr_selection.data.fp = fp;
		break;
	}
}

static inline void subprocess_file_selection_pipe(struct subprocess *p, int file) {
	switch (file) {
	case 0:
		p->_stdin_selection.type = SP_FILE_PIPE;
		break;
	case 1:
		p->_stdout_selection.type = SP_FILE_PIPE;
		break;
	case 2:
		p->_stderr_selection.type = SP_FILE_PIPE;
		break;
	}
}


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

extern struct subprocess* subprocess_init(struct subprocess *p);

/* subprocess icin arguman listesi olusturur.
 * dondurdugu pointer bir subprocess veriyapisi ile kullanilmissa free ile silinmemeli
 * subprocess_free icinde zaten siliniyor */
extern char **subprocess_argv(const char *arg, ...);

/* process baslatir */
extern int subprocess_popen(struct subprocess *p, const char *path, char **argv);

/* subprocess veriyapilarini bellekten siler
   sonlanmamis processlerde hata (-1) dondurur */
extern int subprocess_free(struct subprocess *p);

/* process ile iletisim icin kullanilan bir dosyayi kapatabilir */
extern int subprocess_fclose(struct subprocess *p, FILE* fd);

/* wait & return code */
extern int subprocess_wait(struct subprocess *p);

extern char* buffered_read(int fd, size_t readsize, size_t maxcount);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* _SUBPROCESS_H_ */
