#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <assert.h>
#include <stdlib.h>

#include "subprocess.h"

static void error(const char *e);
void test_1();
void test_2();
void test_3();

int main() {
	test_1();
	test_2();
	test_3();
	return 0;
}

void test_1() {
	int r;
	char *buf;
	struct subprocess process;

	subprocess_init(&process);
	/* subprocess'in stdout'unu processe yonlendir */
	subprocess_file_selection_pipe(&process, 1);
	r = subprocess_popen(&process, "/bin/echo", subprocess_argv("echo", "deneme 1234", NULL));
	if (r < 0)
		error("subprocess_popen() error");

	/* subprocess'in stdin ve stderr'ine erisilemez, stdout'una erisilebilir */
	assert(process.stdin == NULL);
	assert(process.stdout != NULL);
	assert(process.stderr == NULL);

	/* subprocess'in stdout'undan okuma yap */
	buf = buffered_read(process.stdout->_fileno, 100, 100);

	/* echo deneme 1234, "deneme 1234\n" stringi uretmeli */
	r = strcmp(buf, "deneme 1234\n");
	assert(r == 0);

	/* subprocess'in tamamlanmasini bekle */
	int status = subprocess_wait(&process);
	/* echo deneme 1234, 0 degeri dondurmeli */
	assert(status == 0);

	/* subprocess veriyapisini temizle */
	r = subprocess_free(&process);
	/* veri yapisi duzgun bir sekilde temizlendiyse 0 doner */
	assert(r == 0);
}

void test_2() {
	int r;
	struct subprocess process;

	subprocess_init(&process);
	/* subprocess'de stdin olarak /proc/cmdline dosyasini kullan */
	subprocess_file_selection_name(&process, 0, "/proc/cmdline");
	/* stdout ve stderr tanimlanmadigi icin parent process'in stdout ve stderrini kullanir */
	r = subprocess_popen(&process, "/bin/cat", subprocess_argv("cat", NULL));
	if (r < 0)
		error("subprocess_popen() error");

	/* subprocess'in stdin, stdout ve stderr'ine erisilemez */
	assert(process.stdin == NULL);
	assert(process.stdout == NULL);
	assert(process.stderr == NULL);

	/* tamamlanmasini bekle */
	int status = subprocess_wait(&process);
	/* duzgun bir sekilde tamamlanmali */
	assert(status == 0);

	/* temizle */
	r = subprocess_free(&process);
	assert(r == 0);
}

void test_3() {
	int r;
	struct subprocess process;

	subprocess_init(&process);
	/* subprocess'de stdin'i pipe olarak kullan (parent processden yazilabilir) */
	subprocess_file_selection_pipe(&process, 0);
	/* subprocess'in stdout'unu output.txt, stderr'ini error.txt dosyalarina yonlendir */
	subprocess_file_selection_name(&process, 1, "output.txt");
	subprocess_file_selection_name(&process, 2, "error.txt");
	r = subprocess_popen(&process, "/bin/cat", subprocess_argv("cat", NULL));
	if (r < 0)
		error("subprocess_popen() error");

	/* subprocessin stdin'ine erisilebilir, stdout ve stderr'e erisilemez */
	assert(process.stdin != NULL);
	assert(process.stdout == NULL);
	assert(process.stderr == NULL);

	/* subprocess'in stdin'inie deneme yaz */
	fprintf(process.stdin, "deneme\n");

	/* subprocess'in stdin dosyasini kapat */
	r = subprocess_fclose(&process, process.stdin);
	assert(r == 0);

	/* sub processin tamamlanmasini bekle */
	int status = subprocess_wait(&process);

	/* temizle */
	r = subprocess_free(&process);
	assert(r == 0);

	printf("test3 OK, process status: %d\n", status);
}


static void error(const char *e) {
	printf("%s\n", e);
	exit(0);
}
